import glob
import csv
import json
import arrow

file_list = glob.glob('./data/datesortedFullData/*.csv')
print(file_list)

i = 0
for file_item in file_list:
    file_name = file_item.split('\n')
    print(file_name)
    file_name_imp_raw = file_name[0].split('/')[3]
    file_name_imp = file_name_imp_raw.split('.')[0]
    print(file_name_imp)

    ipfileFD = open(file_item, 'r', encoding='UTF-8')
    ipfileBridge = csv.DictReader(ipfileFD)

    opfile_name = './data/top1000tweets/'+file_name_imp+'.txt'
    opfileFD = open(opfile_name, 'w', encoding='UTF-8')

    for tweet in ipfileBridge:
        text = tweet['text']
        opfileFD.write(text)
        opfileFD.write('\n')

    print(i)
    i = i + 1

