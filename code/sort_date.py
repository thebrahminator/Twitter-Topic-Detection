import glob
import csv
import json
import arrow

file_list = glob.glob('./data/sortedFullData/*.csv')
print(file_list)

i = 0
roundDate = "2017-07-19T10:44:23+00:00"
for file_item in file_list:
    file_name = file_item.split('\n')
    print(file_name)
    file_name_imp = file_name[0].split('/')[3]
    print(file_name_imp)

    profileFileFD = open(file_item, 'r', encoding='UTF-8')
    profileFileBridge = csv.DictReader(profileFileFD)

    opfile_full = './data/datesortedFullData/'+file_name_imp
    opfileFD = open(opfile_full, 'w', encoding='UTF-8')
    headers = ('id', 'text', 'created_at', 'like', 'retweet', 'attn_factor', 'elapsedtime')
    opfileBridge = csv.DictWriter(opfileFD, headers)
    opfileBridge.writeheader()
    allTweetList = []

    for tweet in profileFileBridge:
        inter_tweet = {}
        inter_tweet['like'] = tweet['like']
        inter_tweet['retweet'] = tweet['retweet']
        inter_tweet['attn_factor'] = float(tweet['attn_factor'])
        inter_tweet['text'] = tweet['text']
        inter_tweet['id'] = tweet['id']
        inter_tweet['created_at'] = tweet['created_at']
        publishedDate = arrow.get(tweet['created_at'])
        elapsedtime = arrow.get(roundDate) - publishedDate
        hourselapsed, reminder = divmod(elapsedtime.days, 3600)
        totalhours = elapsedtime.days * 24 + hourselapsed
        totalseconds = totalhours * 3600
        inter_tweet['elapsedtime'] = totalseconds
        allTweetList.append(inter_tweet)

    sortedAllTweetList = sorted(allTweetList, key= lambda k: k['elapsedtime'], reverse=True)

    top1000Tweet = sortedAllTweetList

    for tweet in top1000Tweet:
        opfileBridge.writerow(tweet)
