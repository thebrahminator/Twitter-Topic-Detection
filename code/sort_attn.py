import glob
import csv
import json

file_list = glob.glob('./data/fullData/*.csv')
print(file_list)

i = 0
for file_item in file_list:
    file_name = file_item.split('\n')
    print(file_name)
    file_name_imp = file_name[0].split('/')[3]
    print(file_name_imp)

    profileFileFD = open(file_item, 'r', encoding='UTF-8')
    profileFileBridge = csv.DictReader(profileFileFD)

    opfile_full = './data/sortedfullData/'+file_name_imp
    opfileFD = open(opfile_full, 'w', encoding='UTF-8')
    headers = ('id', 'text', 'created_at', 'like', 'retweet', 'attn_factor')
    opfileBridge = csv.DictWriter(opfileFD, headers)
    opfileBridge.writeheader()
    allTweetList = []

    for tweet in profileFileBridge:
        inter_tweet = {}
        inter_tweet['like'] = tweet['like']
        inter_tweet['retweet'] = tweet['retweet']
        inter_tweet['attn_factor'] = float(tweet['attn_factor'])
        inter_tweet['text'] = tweet['text']
        inter_tweet['id'] = tweet['id']
        inter_tweet['created_at'] = tweet['created_at']
        allTweetList.append(inter_tweet)

    sortedAllTweetList = sorted(allTweetList, key= lambda k: k['attn_factor'], reverse=True)

    top1000Tweet = sortedAllTweetList[:1000]

    for tweet in top1000Tweet:
        opfileBridge.writerow(tweet)