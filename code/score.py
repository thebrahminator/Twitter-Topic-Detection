import glob
import csv
import json

file_list = glob.glob('./data/*\n.csv')
print(file_list)

i = 0
for file_item in file_list:
    file_name = file_item.split('\n')
    print(file_name)
    file_name_imp = file_name[0].split('/')[2]

    profileFileFD = open(file_item, 'r', encoding='UTF-8')
    profileFileBridge = csv.DictReader(profileFileFD)

    opfile_full = './data/fullData/'+file_name_imp+'.csv'
    opfileFD = open(opfile_full, 'w', encoding='UTF-8')
    headers = ('id', 'text', 'created_at', 'like', 'retweet', 'attn_factor')
    opfileBridge = csv.DictWriter(opfileFD, headers)
    opfileBridge.writeheader()

    for tweet in profileFileBridge:
        inter_tweet = {}
        inter_tweet['like'] = tweet['fav']
        inter_tweet['retweet'] = tweet['rt']
        inter_tweet['attn_factor'] = 0.5*float(int(tweet['rt']) + int(tweet['fav']))
        inter_tweet['text'] = tweet['text']
        inter_tweet['id'] = tweet['id']
        inter_tweet['created_at'] = tweet['created_at']
        opfileBridge.writerow(inter_tweet)
    print(i)
    i = i + 1