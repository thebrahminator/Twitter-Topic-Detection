import csv
import json
import glob

files_list = glob.glob('./data/newsArticles/*.txt')

with open('./data/allNews.txt', 'w', encoding='UTF-8') as outfile:
    for file_name in files_list:
        with open(file_name, 'r', encoding='UTF-8') as infile:
            for line in infile:
                outfile.write(line)

infile.close()
outfile.close()