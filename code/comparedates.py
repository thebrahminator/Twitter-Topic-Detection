import glob
import csv
import json
import pprint
import arrow

files_list = glob.glob('./data/sortedfullData/*.csv')
print(files_list)

i = 0
for file_item in files_list:
    file_name = file_item.split('\n')
    print(file_name)
    file_name_imp = file_name[0].split('/')[2]

    profileFileFD = open(file_item, 'r', encoding='UTF-8')
    profileFileBridge = csv.DictReader(profileFileFD)
    maxdate = arrow.now()

    for item in profileFileBridge:
        if i == 0:
            maxdate = arrow.get(item['created_at'])
        else:
            if maxdate > arrow.get(item['created_at']):
                maxdate = arrow.get(item['created_at'])
        i = i + 1

    print(i)
print(maxdate)
#2017-07-19T10:44:23+00:00


